import express from 'express';
import fetch from 'node-fetch';
import cookieSession from 'cookie-session';

const app = express();
const clientId = process.env.REDDIT_CLIENT_ID;
const clientSecret = process.env.REDDIT_CLIENT_SECRET;
const cookieSecret = process.env.COOKIE_SECRET;
const redirect = 'http://localhost:5000/login/callback';
const secretState = 'waffles';

app.use(cookieSession({
	secret: cookieSecret
}))

app.get('/', (req, res) => {
	const token = req.session.token;
	if (token) {
		res.redirect('/saved');
	} else {
		res.send('<a href="/login/reddit">Login</a> to reddit');
	}
});

app.get('/login/reddit', (req, res) => {
	const scope = 'identity history';
	const url = `https://www.reddit.com/api/v1/authorize?client_id=${clientId}&response_type=code&state=${secretState}&redirect_uri=${redirect}&duration=temporary&scope=${scope}`;
	res.redirect(url);
});

async function getAccessToken(code) {
	const res = await fetch('https://www.reddit.com/api/v1/access_token', {
		method: 'POST',
		body: `grant_type=authorization_code&code=${code}&redirect_uri=${redirect}`,
		headers: {
			'Content-type': 'application/x-www-form-urlencoded',
			Authorization: 'Basic ' + Buffer.from(clientId + ':' + clientSecret).toString('base64')
		}
	});
	const data = await res.json();
	return data.access_token;
}

app.get('/login/callback', async (req, res) => {
	const {
		code,
		state,
		error
	 } = req.query;
	if (error) {
		res.send(error);
	}
	if (state !== secretState) {
		res.send('Not Authorized');
	}
	const token = await getAccessToken(code);
	if (token) {
		req.session.token = token;
		res.redirect('/saved')
	} else {
		res.send('Error getting token');
	}
});

app.get('/saved', async (req, res) => {
	const { c, a } = req.query;
	const limit = 50;
	const count = c || 0;
	const token = req.session.token;
	const after = a || '';
	let url = `https://oauth.reddit.com/user/luchosmith/saved?limit=${limit}&count=${count}&after=${after}`;
	// let url = `https://oauth.reddit.com/api/v1/me`;
	console.log(url);
	const response = await fetch(url, {
		headers: {
			Authorization: 'bearer ' + token,
			'User-Agent': 'Mac:lucho_saved_posts:v0.1.1 (by /u/luchosmith)'
		}
	});	
	const data = await response.json();
	res.send(data);
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
	console.log('listening on port: ', port);
});